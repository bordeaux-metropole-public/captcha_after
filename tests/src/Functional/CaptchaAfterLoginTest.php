<?php

namespace Drupal\Tests\captcha_after\Functional;

use Drupal\captcha_after\CaptchaAfterConstantsInterface as CAC;
use Drupal\Tests\captcha\Functional\CaptchaWebTestBase;

/**
 * Testing of the basic CAPTCHA After functionality for login form.
 *
 * @group captcha_after
 */
class CaptchaAfterLoginTest extends CaptchaAfterWebTestBase {

  const LOGIN_HTML_FORM_ID = 'user-login-form';

  /**
   * User login form captcha point.
   *
   * @var \Drupal\captcha\Entity\CaptchaPoint
   */
  protected $userLoginCaptchaPoint;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Configure captcha protection for user login form.
    $this->userLoginCaptchaPoint = $this->initializeCaptchaPoint(
      'user_login_form', 'default');
  }

  /**
   * Testing of submit threshold for user login form.
   */
  public function testCaptchaAfterSubmitThreshold() {

    $assert = $this->assertSession();

    // Test disabling of submit treshold.
    $this->userLoginCaptchaPoint->setThirdPartySetting(
      'captcha_after', 'captcha_after_submit_threshold',
      CAC::THRESHOLD_DISABLED);
    $this->userLoginCaptchaPoint->save();
    $html = $this->drupalGet('user');
    $assert->fieldExists('captcha_response');

    // Test enabling of submit treshold.
    $this->userLoginCaptchaPoint->setThirdPartySetting(
      'captcha_after', 'captcha_after_submit_threshold', 3);
    $this->userLoginCaptchaPoint->save();
    $this->drupalGet('user');
    $assert->fieldNotExists('captcha_response');

    // Test clean login.
    $edit = [
      'name' => $this->testUser1->getAccountName(),
      'pass' => $this->testUser1->pass_raw,
    ];
    $this->drupalGet('user');
    $this->submitForm($edit, $this->t('Log in'), self::LOGIN_HTML_FORM_ID);
    // Successfully redirected to user page.
    $assert->pageTextContains('Member for');
    $this->drupalLogout();

    // Test bad login treshold limit 1.
    $edit['pass'] .= 'wrong pass';
    $this->drupalGet('user');
    $this->submitForm($edit, $this->t('Log in'), self::LOGIN_HTML_FORM_ID);
    $assert->pageTextContains('Unrecognized username or password.');
    $assert->fieldNotExists('captcha_response');

    // Test bad login treshold limit 2.
    $this->drupalGet('user');
    $this->submitForm($edit, $this->t('Log in'), self::LOGIN_HTML_FORM_ID);
    $assert->pageTextContains('Unrecognized username or password.');
    $assert->fieldNotExists('captcha_response');

    // Test bad login treshold limit 3.
    $this->drupalGet('user');
    $this->submitForm($edit, $this->t('Log in'), self::LOGIN_HTML_FORM_ID);
    $assert->pageTextContains('Unrecognized username or password.');
    $assert->fieldExists('captcha_response');

    // Try to login with incorect captcha solution.
    $edit['pass'] = $this->testUser1->pass_raw;
    $edit['captcha_response'] = '?';
    $this->drupalGet('user');
    $this->submitForm($edit, $this->t('Log in'), self::LOGIN_HTML_FORM_ID);
    $assert->pageTextContains(
      CaptchaWebTestBase::CAPTCHA_WRONG_RESPONSE_ERROR_MESSAGE);
    $assert->fieldExists('captcha_response');

    // Try to login with correct captcha solution.
    $edit['captcha_response'] = 'Test 123';
    $this->drupalGet('user');
    $this->submitForm($edit, $this->t('Log in'), self::LOGIN_HTML_FORM_ID);
    $assert->pageTextContains('Member for');
    $assert->fieldNotExists('captcha_response');
  }

}
