<?php

namespace Drupal\Tests\captcha_after\Functional;

use Drupal\captcha\Entity\CaptchaPoint;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Base class for CAPTCHA After tests.
 *
 * Provides common setup stuff and various helper functions.s
 */
abstract class CaptchaAfterWebTestBase extends BrowserTestBase {

  use StringTranslationTrait;

  /**
   * Modules to install for this Test class.
   *
   * @var array
   */
  protected static $modules = [
    'captcha',
    'captcha_test',
    'captcha_after',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test user 1.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $testUser1;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType(['type' => 'page']);

    // Create a first test user.
    $permissions = ['create page content'];
    $this->testUser1 = $this->drupalCreateUser($permissions);

    // Set default test CAPTCHA challenge.
    $this->config('captcha.settings')
      ->set('default_challenge', 'captcha/Test')
      ->save();
  }

  /**
   * Helper function for adding/updating a CAPTCHA point.
   *
   * @param string $form_id
   *   the form ID to configure.
   * @param string $captcha_type
   *   The setting for the given form_id, can be:
   *   - 'default' to use the default challenge type
   *   - NULL to remove the entry for the CAPTCHA type
   *   - something of the form 'image_captcha/Image'
   *   - an object with attributes $captcha_type->module
   *   and $captcha_type->captcha_type.
   */
  protected function initializeCaptchaPoint($form_id, $captcha_type) {
    /** @var \Drupal\captcha\Entity\CaptchaPoint $captcha_point */
    $captcha_point = CaptchaPoint::load($form_id);

    if ($captcha_point) {
      $captcha_point->setCaptchaType($captcha_type);
    }
    else {
      $captcha_point = new CaptchaPoint([
        'formId' => $form_id,
        'captchaType' => $captcha_type,
      ], 'captcha_point');
    }
    $captcha_point->enable();

    $captcha_point->save();

    return $captcha_point;
  }

}
