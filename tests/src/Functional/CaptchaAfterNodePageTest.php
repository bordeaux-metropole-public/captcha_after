<?php

namespace Drupal\Tests\captcha_after\Functional;

use Drupal\captcha_after\CaptchaAfterConstantsInterface as CAC;
use Drupal\Tests\captcha\Functional\CaptchaWebTestBase;

/**
 * Testing of the basic CAPTCHA After functionality for page creation.
 *
 * @group captcha_after
 */
class CaptchaAfterNodePageTest extends CaptchaAfterWebTestBase {

  const PAGE_NODE_HTML_FORM_ID = 'node-page-form';

  /**
   * Add node page form captcha point.
   *
   * @var \Drupal\captcha\Entity\CaptchaPoint
   */
  protected $nodePageFormCaptchaPoint;

  /**
   * Test user 2.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $testUser2;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create a second test user.
    $permissions = ['create page content'];
    $this->testUser2 = $this->drupalCreateUser($permissions);

  }

  /**
   * Testing of flooding thresholds for node/add/page form.
   */
  public function testCaptchaAfterFloodingThresholds() {
    // Order of execution is important, that's why we are calling manually
    // these methods.
    $this->doTestCaptchaAfterFloodingThreshold();
    $this->doTestCaptchaAfterGlobalFloodingThreshold();
  }

  /**
   * Testing of flooding threshold for node/add/page form.
   */
  public function doTestCaptchaAfterFloodingThreshold() {

    $assert = $this->assertSession();

    // Login test user.
    $this->drupalLogin($this->testUser1);

    // Test clean page post without captcha.
    $edit = [
      'title[0][value]' => $this->randomMachineName(8),
      'body[0][summary]' => $this->randomString(32),
    ];
    $this->drupalGet('node/add/page');
    // One submit with captcha_after disabled.
    $this->submitForm($edit, $this->t('Save'));
    $assert->pageTextContains($this->t('Page @title has been created', ['@title' => $edit['title[0][value]']]));

    // Turn on captcha protection for node/add/page form.
    $this->nodePageFormCaptchaPoint = $this->initializeCaptchaPoint(
      'node_page_form', 'default');

    // Check default captcha protection.
    $this->drupalGet('node/add/page');
    $assert->fieldExists('captcha_response');

    // Test skipping of all checks, we should see captcha response in this case.
    $this->nodePageFormCaptchaPoint->setThirdPartySetting(
      'captcha_after', 'captcha_after_submit_threshold',
      CAC::THRESHOLD_DISABLED);
    $this->nodePageFormCaptchaPoint->setThirdPartySetting(
      'captcha_after', 'captcha_after_session_submit_threshold',
      CAC::THRESHOLD_DISABLED);
    $this->nodePageFormCaptchaPoint->setThirdPartySetting(
      'captcha_after', 'captcha_after_global_submit_threshold',
      CAC::THRESHOLD_DISABLED);
    $this->nodePageFormCaptchaPoint->setThirdPartySetting(
      'captcha_after', 'captcha_after_flooding_threshold',
      CAC::THRESHOLD_DISABLED);
    $this->nodePageFormCaptchaPoint->setThirdPartySetting(
      'captcha_after', 'captcha_after_global_flooding_threshold',
      CAC::THRESHOLD_DISABLED);
    $this->nodePageFormCaptchaPoint->save();
    $this->drupalGet('node/add/page');
    $assert->fieldExists('captcha_response');

    // Test flooding threshold. We need to set submit and global thresholds also
    // to not equal to 0.
    $this->nodePageFormCaptchaPoint->setThirdPartySetting(
      'captcha_after', 'captcha_after_submit_threshold', 3);
    $this->nodePageFormCaptchaPoint->setThirdPartySetting(
      'captcha_after', 'captcha_after_flooding_threshold', 2);
    $this->nodePageFormCaptchaPoint->setThirdPartySetting(
      'captcha_after', 'captcha_after_global_flooding_threshold', 1000);
    $this->nodePageFormCaptchaPoint->save();
    $this->drupalGet('node/add/page');
    $assert->fieldNotExists('captcha_response');

    $this->drupalGet('node/add/page');
    // First submit with captcha_after enabled (valid).
    $this->submitForm($edit, $this->t('Save'));
    $assert->pageTextContains($this->t('Page @title has been created', ['@title' => $edit['title[0][value]']]));

    $this->drupalGet('node/add/page');
    $assert->fieldNotExists('captcha_response');

    $this->drupalGet('node/add/page');
    // Second submit (valid).
    $this->submitForm($edit, $this->t('Save'));
    $assert->pageTextContains($this->t('Page @title has been created', ['@title' => $edit['title[0][value]']]));

    $this->drupalGet('node/add/page');
    $assert->fieldExists('captcha_response');

    // Try with bad captcha solution.
    $edit['captcha_response'] = '?';
    $this->drupalGet('node/add/page');
    // Third submit (invalid).
    $this->submitForm($edit, $this->t('Save'));
    $assert->pageTextContains(CaptchaWebTestBase::CAPTCHA_WRONG_RESPONSE_ERROR_MESSAGE);
    $assert->fieldExists('captcha_response');

    // Try with good captcha solution.
    $edit['captcha_response'] = 'Test 123';
    $this->drupalGet('node/add/page');
    // Fourth submit (valid).
    $this->submitForm($edit, $this->t('Save'));
    $assert->pageTextContains($this->t('Page @title has been created', ['@title' => $edit['title[0][value]']]));
  }

  /**
   * Testing of global flooding threshold.
   */
  public function doTestCaptchaAfterGlobalFloodingThreshold() {

    $assert = $this->assertSession();

    // Test global flooding threshold.
    $this->nodePageFormCaptchaPoint->setThirdPartySetting(
      'captcha_after', 'captcha_after_submit_threshold', 3);
    // While testing we are on a same ip so this needs to be bigger then global
    // flooding value.
    $this->nodePageFormCaptchaPoint->setThirdPartySetting(
      'captcha_after', 'captcha_after_flooding_threshold', 10);
    // Six global submits per hour.
    $this->nodePageFormCaptchaPoint->setThirdPartySetting(
      'captcha_after', 'captcha_after_global_flooding_threshold', 6);
    $this->nodePageFormCaptchaPoint->save();

    // Let's test a few more submits with other test user.
    $this->drupalLogin($this->testUser2);

    $this->drupalGet('node/add/page');
    // Only four submits done, captcha shouldn't be displayed, threshold is 6.
    $assert->fieldNotExists('captcha_response');

    $edit = [
      'title[0][value]' => $this->randomMachineName(8),
      'body[0][summary]' => $this->randomString(32),
    ];

    $this->drupalGet('node/add/page');
    // Fifth submit (valid).
    $this->submitForm($edit, $this->t('Save'));
    $assert->pageTextContains($this->t('Page @title has been created', ['@title' => $edit['title[0][value]']]));

    $this->drupalGet('node/add/page');
    // Only five submits done, captcha shouldn't be displayed, threshold is 6.
    $assert->fieldNotExists('captcha_response');

    $this->drupalGet('node/add/page');
    // Sixth submit (valid).
    $this->submitForm($edit, $this->t('Save'));
    $assert->pageTextContains($this->t('Page @title has been created', ['@title' => $edit['title[0][value]']]));

    $this->drupalGet('node/add/page');
    $assert->fieldExists('captcha_response');

    // Try with bad captcha solution.
    $edit['captcha_response'] = '?';
    $this->drupalGet('node/add/page');
    // Seventh submit (invalid).
    $this->submitForm($edit, $this->t('Save'));
    $assert->pageTextContains(CaptchaWebTestBase::CAPTCHA_WRONG_RESPONSE_ERROR_MESSAGE);
    $assert->fieldExists('captcha_response');

    // Try with good captcha solution.
    $edit['captcha_response'] = 'Test 123';
    $this->drupalGet('node/add/page');
    // Eighth submit (valid).
    $this->submitForm($edit, $this->t('Save'));
    $assert->pageTextContains($this->t('Page @title has been created', ['@title' => $edit['title[0][value]']]));

    // We have exceeded the 6 submits per hour global threshold (8 submits).
    $this->drupalGet('node/add/page');
    $assert->fieldExists('captcha_response');
  }

}
