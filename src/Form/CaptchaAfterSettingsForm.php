<?php

namespace Drupal\captcha_after\Form;

use Drupal\captcha_after\CaptchaAfterConstantsInterface as CAC;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Displays the captcha after settings form.
 *
 * @package Drupal\captcha_after\Form
 */
class CaptchaAfterSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['captcha_after.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'captcha_after_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('captcha_after.settings');

    // Get captcha_after settings.
    $submit_threshold = $config->get('submit_threshold');
    $session_submit_threshold = $config->get('session_submit_threshold');
    $global_submit_threshold = $config->get('global_submit_threshold');
    $flooding_threshold = $config->get('flooding_threshold');
    $global_flooding_threshold = $config->get('global_flooding_threshold');

    $form['global_submit_threshold'] = [
      '#type' => 'number',
      '#title' => t('Default CAPTCHA global submit threshold'),
      '#description' => t('Number of times <strong>ALL</strong> visitors are
      allowed to submit non-valid data into the form within an hour before starting to
      protect form with CAPTCHA.<br>Enter 0 to disable this behaviour.'),
      '#default_value' => $global_submit_threshold ?: CAC::THRESHOLD_DISABLED,
      '#required' => TRUE,
      '#min' => 0,
    ];

    $form['submit_threshold'] = [
      '#type' => 'number',
      '#title' => t('Default CAPTCHA submit threshold'),
      '#description' => t('Number of times a user (based on hostname/IP) is permitted
      to submit non-valid data into the form before starting to protect form with CAPTCHA.
      <br>Enter 0 to disable this behaviour.'),
      '#default_value' => $submit_threshold ?: CAC::THRESHOLD_DISABLED,
      '#required' => TRUE,
      '#min' => 0,
    ];

    $form['session_submit_threshold'] = [
      '#type' => 'number',
      '#title' => t('Default CAPTCHA session submit threshold'),
      '#description' => t('Number of times a user (based on session ID) is permitted
      to submit non-valid data into the form before starting to protect form with CAPTCHA.
      <br>Enter 0 to disable this behaviour.'),
      '#default_value' => $session_submit_threshold ?: CAC::THRESHOLD_DISABLED,
      '#required' => TRUE,
      '#min' => 0,
    ];

    $form['global_flooding_threshold'] = [
      '#type' => 'number',
      '#title' => t('Default CAPTCHA global flooding threshold'),
      '#description' => t('Number of times <strong>ALL</strong> visitors are
      allowed to submit a protected form within an hour before starting to
      protect form with CAPTCHA. This is useful for protecting against flooding
      from multiple IPs.<br>Enter 0 to disable this behaviour.'),
      '#default_value' => $global_flooding_threshold ?: CAC::THRESHOLD_DISABLED,
      '#required' => TRUE,
      '#min' => 0,
    ];

    $form['flooding_threshold'] = [
      '#type' => 'number',
      '#title' => t('Default CAPTCHA flooding threshold'),
      '#description' => t('Number of times a visitor (based on hostname/IP) is
      allowed to submit a protected form in an hour before starting to protect
      form with CAPTCHA. This is useful for protecting against repeated (but
      valid) submissions.<br>Enter 0 to disable this behaviour.'),
      '#default_value' => $flooding_threshold ?: CAC::THRESHOLD_DISABLED,
      '#required' => TRUE,
      '#min' => 0,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('captcha_after.settings');

    $config->set('submit_threshold', $form_state->getValue('submit_threshold'));
    $config->set('session_submit_threshold', $form_state->getValue('session_submit_threshold'));
    $config->set('global_submit_threshold', $form_state->getValue('global_submit_threshold'));
    $config->set('flooding_threshold', $form_state->getValue('flooding_threshold'));
    $config->set('global_flooding_threshold', $form_state->getValue('global_flooding_threshold'));

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
