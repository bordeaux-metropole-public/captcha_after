<?php

namespace Drupal\captcha_after;

/**
 * Defines a few constants for the module.
 */
interface CaptchaAfterConstantsInterface {

  /**
   * Threshold value indicating to replace it by the module default value.
   */
  const THRESHOLD_DEFAULT = '';

  /**
   * Threshold value indicating that it shouldn't be taken into account.
   */
  const THRESHOLD_DISABLED = '0';

  /**
   * Flood event default expiration delay in seconds.
   */
  const FLOOD_EXPIRATION = 3600;

}
