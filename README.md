# SUMMARY

Show CAPTCHA protection on selected forms after specified number of unsuccessful
form submit attempts has been made.


# REQUIREMENTS

CAPTCHA module
http://drupal.org/project/captcha


# INSTALLATION

* Install as usual, see http://drupal.org/node/70151 for further information.


# CONFIGURATION

* Visit **Configuration** > **People** > **CAPTCHA After settings** and
  configure global thresholds.
* Visit **Configuration** > **People** > **CAPTCHA module settings**, then click
  on the **Form settings** tab and configure the **CAPTCHA After** thresholds
  for all the forms that require this functionality by clicking on the
  corresponding **Edit** action link.


# CONTACT

## Current maintainers:
* Puljic Ivica (pivica) - http://drupal.org/user/41070

## Contributors:
* Matt Vance (Matt V.) - http://drupal.org/user/88338
* Sophie Shanahan-Kluth - sophie.sk@focusrite.com
* Rohit Joshi - https://medium.com/@joshirohit100
